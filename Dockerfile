FROM python:3.6

ENV TERM xterm-256color

RUN apt-get update \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install colorama
ADD colors.py /colors.py
